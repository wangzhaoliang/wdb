该代码主要作用是，在类脑平台中利用wdb调试深度学习代码时，实现自动化添加断点调试。

1）add_line.py

假设需要debug的文件是mnist.py,通过add_line.py,会复制一份文件mnist_bak.py,并在mnist_bak.py中的第4行和第5行中插入（import wdb 和wdb.set_trace()）,插入的行号可以在代码中修改。自动在第4行和第5行中插入调试代码到mnist_bak.py后如下图所示。

2）mnist.py

需要deug的文件。

3）sh.py

sh.py负责执行add_line.py的命令、执行python mnist_bak.py进行debug,以及debug结束后删除备份文件。

4）类脑平台json文件

wdb.json.
